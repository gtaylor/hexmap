part of unit_store;


class BattleSnakeClient extends WampClientProtocol {
  UnitStore unitStore;

  static final Map EVENTS = {
    "http://hexmap.com/unit/events/detected": {
      'handler': BSClientPubSubHandler.handleUnitDetected,
    },
    "http://hexmap.com/unit/events/destroyed": {
      'handler': BSClientPubSubHandler.handleUnitDestroyed,
    },
    "http://hexmap.com/unit/events/stale-removal": {
      'handler': BSClientPubSubHandler.handleUnitStaleRemoval,
    },
    "http://hexmap.com/unit/events/hit-landed": {
      'handler': BSClientPubSubHandler.handleShotLanded,
    },
    "http://hexmap.com/unit/events/shot-missed": {
      'handler': BSClientPubSubHandler.handleShotMissed,
    },
    "http://hexmap.com/unit/events/state-changed": {
      'handler': BSClientPubSubHandler.handleUnitStateChanged,
    }
  };

  BattleSnakeClient(socket, UnitStore this.unitStore) : super(socket) {

  }

  onOpenSession() {
    subscribe('http://hexmap.com/unit/events/detected');
    subscribe('http://hexmap.com/unit/events/destroyed');
    subscribe('http://hexmap.com/unit/events/stale-removal');
    subscribe('http://hexmap.com/unit/events/hit-landed');
    subscribe('http://hexmap.com/unit/events/shot-missed');
    subscribe('http://hexmap.com/unit/events/state-changed');

    call("http://hexmap.com/unit-store#get", [])
      .then((response) => BSClientRPCHandler.handleUnitStoreGet(response, unitStore));
  }

  onEvent(topicUri, event) {
    //print("Event received ${topicUri}");
    var handlerFunc = EVENTS[topicUri]['handler'];
    handlerFunc(event, unitStore);
  }
}


class BSClientPubSubHandler {

  static void handleUnitDetected(event, UnitStore unitStore) {
    print("Handling unit detected ${event}");
    Map uDat = JSON.decode(event['unit']);
    Unit unit = BSUnitParser.parseUnitJson(uDat);
    unitStore.addNewUnit(unit);
  }

  static void handleUnitDestroyed(event, UnitStore unitStore) {
    print("Handling unit destroyed ${event}");
    unitStore.markUnitAsDestroyedById(event['victim_id']);
  }

  static void handleUnitStaleRemoval(event, UnitStore unitStore) {
    print("Handling unit stale removal ${event}");
    unitStore.removeUnitById(event['unit_id']);
  }

  static void handleShotLanded(event, UnitStore unitStore) {
    print("Handling shot landed ${event}");
    unitStore.recordHit(
        event['victim_id'], event['aggressor_id'], event['weapon_name']);
  }

  static void handleShotMissed(event, UnitStore unitStore) {
    print("Handling shot missed ${event}");
    unitStore.recordMiss(
        event['victim_id'], event['aggressor_id'], event['weapon_name']);
  }

  static void handleUnitStateChanged(event, UnitStore unitStore) {
    print("Handling state change ${event}");
    Map uDat = JSON.decode(event['unit']);
    Unit unit = BSUnitParser.parseUnitJson(uDat);
    unitStore.updateUnit(unit);
  }

}


class BSClientRPCHandler {

  static void handleUnitStoreGet(response, UnitStore unitStore) {
    Map unitStoreJson = JSON.decode(response);
    unitStoreJson.values.forEach((uDat) {
      Unit unit = BSUnitParser.parseUnitJson(uDat);
      unitStore.addNewUnit(unit);
    });
  }

}


class BSUnitParser {
  static Unit parseUnitJson(Map uDat) {
    DateTime lastSeen = DateTime.parse(uDat['last_seen']);
    Unit unit = new Unit(
        uDat['contact_id'], uDat['unit_type'], uDat['mech_name'],
        uDat['x_coord'], uDat['y_coord'], uDat['z_coord'],
        uDat['speed'], uDat['heading'], uDat['jump_heading'],
        uDat['range_to_hex_center'], uDat['bearing_to_hex_center'],
        uDat['tonnage'], uDat['heat'], uDat['status'], lastSeen
    );
    return unit;
  }
}