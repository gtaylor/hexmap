part of unit_store;

final EventType<Unit> unitAddedEvent = new EventType<Unit>();
final EventType<Unit> unitUpdatedEvent = new EventType<Unit>();
final EventType<String> unitRemovedEvent = new EventType<String>();
final EventType<String> unitDestroyedEvent = new EventType<String>();
final EventType<Map> shotLanded = new EventType<Map>();
final EventType<Map> shotMissed = new EventType<Map>();

class UnitStore {
  Map<String,Unit> _units;
  EventBus eventBus;

  UnitStore(this.eventBus) {
    _units = new Map<String,Unit>();

    //eventBus.on(newUnitAddedEvent).listen((Unit unit) {
    //  print(unit);
    //});
  }

  getAllUnits() {
    return _units.values;
  }

  void addNewUnit(Unit unit) {
    print("Adding ${unit}");
    _units[unit.contactId] = unit;
    eventBus.fire(unitAddedEvent, unit);
  }

  void updateUnit(Unit unit) {
    print("Updating ${unit}");
    _units[unit.contactId] = unit;
    eventBus.fire(unitUpdatedEvent, unit);
  }

  void markUnitAsDestroyedById(String unitId) {
    eventBus.fire(unitDestroyedEvent, unitId);
    removeUnitById(unitId);
  }

  void removeUnitById(String unitId) {
    print("Removing ${unitId}");
    _units.remove(unitId);
    eventBus.fire(unitRemovedEvent, unitId);
  }

  void recordHit(String victimId, String aggressorId, String weaponName) {
    Map shot = {
      'victimId': victimId,
      'aggressorId': aggressorId,
      'weaponName': weaponName,
    };
    eventBus.fire(shotLanded, shot);
  }

  void recordMiss(String victimId, String aggressorId, String weaponName) {
    Map shot = {
      'victimId': victimId,
      'aggressorId': aggressorId,
      'weaponName': weaponName,
    };
    eventBus.fire(shotMissed, shot);
  }
}

class Unit {
  String contactId;
  String unitType;
  String mechName;
  int x_coord;
  int y_coord;
  int z_coord;
  double speed;
  int heading;
  int jump_heading;
  double rangeToHexCenter;
  int bearingToHexCenter;
  int tonnage;
  String heat;
  String status;

  DateTime lastSeen;

  Unit(
    this.contactId, this.unitType, this.mechName, this.x_coord,
    this.y_coord, this.z_coord, this.speed, this.heading, this.jump_heading,
    this.rangeToHexCenter, this.bearingToHexCenter, this.tonnage,
    this.heat, this.status, this.lastSeen);

  String toString() {
    return "[${contactId}] ${mechName}";
  }
}
