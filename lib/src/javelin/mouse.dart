part of javelin;

/*

  Copyright (C) 2012 John McCutchan <john@johnmccutchan.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

*/

class JavelinMouseButtonCodes {
  static final int MouseButtonLeft = 0;
  static final int MouseButtonMid = 1;
  static final int MouseButtonRight = 2;

  static final int NumMouseButtonCodes = 3;
}

class JavelinMouse {
  List<bool> _buttons;
  double _accumDX;
  double _accumDY;
  double _scrollDX = 0.0;
  double _scrollDY = 0.0;
  double _X;
  double _Y;
  double _prevX;
  double _prevY;
  bool locked;

  JavelinMouse() {
    _accumDX = 0.0;
    _accumDY = 0.0;
    _scrollDX = 0.0;
    _scrollDY = 0.0;
    _X = 0.0;
    _Y = 0.0;
    _buttons = new List<bool>(JavelinMouseButtonCodes.NumMouseButtonCodes);
    for (int i = 0; i < JavelinMouseButtonCodes.NumMouseButtonCodes; i++) {
      _buttons[i] = false;
    }
    locked = false;
  }

  bool pressed(int buttonCode) {
    return _buttons[buttonCode];
  }

  mouseButtonEvent(int buttonCode, bool down) {
    _buttons[buttonCode] = down;
  }

  mouseMoveEvent(MouseEvent event) {
    _X = event.stageX;
    _Y = event.stageY;

    if(pressed(JavelinMouseButtonCodes.MouseButtonLeft)) {
      if(_prevX != null) {
        _accumDX += _prevX - X;
        _accumDY += _prevY - Y;
      }
      _prevX = _X;
      _prevY = _Y;
    } else {
      _prevX = null;
      _prevY = null;
    }

    //print("X: ${_X} Y: ${_Y} _accumDX ${event.deltaX} _accumDY ${_accumDY}");
  }

  mouseScrollEvent(MouseEvent event) {
    _scrollDX = event.deltaX;
    _scrollDY = event.deltaY;
  }

  double get accumulatedDX => _accumDX;
  double get accumulatedDY => _accumDY;
  double get scrollDX => _scrollDX;
  double get scrollDY => _scrollDY;

  double get X => _X;
  double get Y => _Y;

  void resetAccumulator() {
    _accumDX = 0.0;
    _accumDY = 0.0;
    _scrollDX = 0.0;
    _scrollDY = 0.0;
  }
}