part of map_widget;

class UnitLayerSprite extends Sprite {
  UnitStore unitStore;
  EventBus eventBus;

  MapContainer mapContainer;
  TerrainLayerSprite terrainLayer;

  Map<String,UnitSprite> _unitSprites;

  UnitLayerSprite(MapContainer this.mapContainer, UnitStore this.unitStore) {
    eventBus = mapContainer.eventBus;
    terrainLayer = mapContainer.terrainLayer;
    _unitSprites = new Map<String,UnitSprite>();
    x = 0.0;
    y = 0.0;
    this.addTo(mapContainer);

    eventBus.on(unitAddedEvent).listen(addUnit);
    eventBus.on(unitUpdatedEvent).listen(updateUnit);
  }

  void addAllFromStore() {
    unitStore.getAllUnits().forEach((unit) {
      addUnit(unit);
    });
  }

  void addUnit(Unit unit) {
    _unitSprites[unit.contactId] = new UnitSprite(unit, this);
  }

  void updateUnit(Unit unit) {
    _unitSprites[unit.contactId].updateFromUnit(unit);
  }
}


class UnitSprite {
  UnitLayerSprite unitLayer;
  TextureAtlas _textureAtlas;
  Unit unit;
  Bitmap bitmap;

  UnitSprite(Unit this.unit, UnitLayerSprite this.unitLayer) {
    _textureAtlas = unitLayer.mapContainer.resourceManager.getTextureAtlas('unitTextures');
    var bitmapData = _textureAtlas.getBitmapData('Anubis');
    var coords = unitLayer.terrainLayer.getPixelsFromHexCoords(unit.x_coord, unit.y_coord);
    bitmap = new Bitmap(bitmapData);
    bitmap.pivotX = bitmap.width / 2.0;
    bitmap.pivotY = bitmap.height / 2.0;
    bitmap.x = coords['hexUpperLeftXPixel'] + bitmap.pivotX;
    bitmap.y = coords['hexUpperLeftYPixel'] + bitmap.pivotY;
    bitmap.rotation = unit.heading * (math.PI / 180);
    bitmap.addTo(unitLayer);

  }

  void updateFromUnit(newUnit) {
    var coords = unitLayer.terrainLayer.getPixelsFromHexCoords(
        newUnit.x_coord, newUnit.y_coord);
    bitmap.x = coords['hexUpperLeftXPixel'] + bitmap.pivotX;
    bitmap.y = coords['hexUpperLeftYPixel'] + bitmap.pivotY;
    bitmap.rotation = newUnit.heading * (math.PI / 180);
  }
}