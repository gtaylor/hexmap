part of map_widget;

/**
 * This class handles all drawing on the map. Currently only hexes.
 */
class TerrainLayerSprite extends Sprite {
  static const double HEX_PIXEL_SIZE = 30.0;
  // The height AND width of each region, in hexes.
  static const int regionSize = 35;
  int regionsWide;
  int regionsTall;
  List<List> mapRegions;
  double regionPixelWidth;
  double regionPixelHeight;

  double pixelsWide;
  double pixelsTall;
  double hexPixelHeight;
  double hexPixelWidth;
  double hexHorizOffset;
  double hexVertOffset;

  MapContainer mapContainer;
  TextureAtlas _textureAtlas;
  // The current region that the viewport is centered on.
  TerrainLayerRegionSprite currentVisibleRegion;

  TerrainLayerSprite(MapContainer this.mapContainer) {
    hexPixelWidth = HEX_PIXEL_SIZE * 2.0;
    hexPixelHeight = math.sqrt(3) /2 * hexPixelWidth;
    hexVertOffset = 3/4 * hexPixelHeight;
    hexHorizOffset = hexPixelWidth * 3/4;

    x = 0.0;
    y = 0.0;
    this.addTo(mapContainer);

    mapRegions = new List<List>();
    regionPixelWidth = regionSize * hexHorizOffset;
    regionPixelHeight = regionSize * hexPixelHeight;
  }

  Map getPixelsFromHexCoords(int x, int y) {
    double hexUpperLeftXPixel = x * hexHorizOffset;
    double hexUpperLeftYPixel = y * hexPixelHeight;
    bool hexIsEven = (x & 1) == 0;
    if (hexIsEven) {
      hexUpperLeftYPixel += hexPixelHeight / 2;
    }
    return {
      'hexUpperLeftXPixel': hexUpperLeftXPixel,
      'hexUpperLeftYPixel': hexUpperLeftYPixel,
    };
  }

  double calcAndSetPixelDimensions() {
    // For some reason, we're off by a bit with this.
    pixelsWide = (mapContainer.hexesWide * hexHorizOffset) + 18.0;
    pixelsTall = (mapContainer.hexesTall * hexPixelHeight) + 28.0;
  }

  void createRegions() {
    _textureAtlas = mapContainer.resourceManager.getTextureAtlas('hexTextures');

    // ~/ is truncating division.
    regionsWide = (mapContainer.hexesWide / regionSize).ceil();
    regionsTall = (mapContainer.hexesTall / regionSize).ceil();

    for(var y = 0; y < regionsTall; y++) {
      mapRegions.add([]);
      for(var x = 0; x < regionsWide; x++) {
        TerrainLayerRegionSprite mapRegion = new TerrainLayerRegionSprite(this, x, y);
        mapRegion.drawAllHexes();
        mapRegions[y].add(mapRegion);
      }
    }
    toggleRegions();
  }

  TerrainLayerRegionSprite getCurrentRegion() {
    int regionX = math.min(mapContainer.x.abs() ~/ regionPixelWidth.toInt(), regionsWide - 1);
    int regionY = math.min(mapContainer.y.abs() ~/ regionPixelHeight.toInt(), regionsTall - 1);
    //print("Current region: ${regionX},${regionY}");
    return mapRegions[regionY][regionX];
  }

  void toggleRegions() {
    TerrainLayerRegionSprite newVisibleRegion = getCurrentRegion();
    // Avoid all of these calculations if the curent visible region
    // hasn't changed since the last time we toggled regions.
    if(newVisibleRegion == currentVisibleRegion)
      return;
    currentVisibleRegion = newVisibleRegion;

    for(var y = 0; y < regionsTall; y++) {
      for(var x = 0; x < regionsWide; x++) {
        mapRegions[y][x].showIfNearRegion(currentVisibleRegion);
      }
    }
  }

  void panX(double pixels) {
    mapContainer.pivotX += pixels;
  }

  void panY(double pixels) {
    mapContainer.pivotY += pixels;
  }


  /**
   * scalePoint is currently unused, but will eventually be used to scale
   * in/out based on mouse pointer location. Is this my pivotX/pivotY?
   */
  void modScale(Point scalePoint, double scaleAmount) {
    //if(scaleAmount + mapContainer.scaleX > 1.5)
    //  return;
    if(scaleAmount + mapContainer.scaleX < 0.6)
      return;

    mapContainer.scaleX += scaleAmount;
    mapContainer.scaleY += scaleAmount;
  }
}


/**
 * The terrain layer is broken up into regions, which are rectangular.
 * We dynamically show/hide these to make sure we have a reasonable amount
 * of stuff being rendered on the Stage at any given time.
 */
class TerrainLayerRegionSprite extends Sprite {
  TerrainLayerSprite terrainLayer;
  MapContainer mapContainer;
  int regionX;
  int regionY;
  int numHexesWide;
  int numHexesTall;

  double regionStartXPixel;
  double regionStartYPixel;

  bool isShown = false;

  TerrainLayerRegionSprite(
      TerrainLayerSprite this.terrainLayer, int this.regionX, int this.regionY) {
    mapContainer = terrainLayer.mapContainer;
    numHexesWide = TerrainLayerSprite.regionSize;
    numHexesTall = TerrainLayerSprite.regionSize;
    regionStartXPixel = regionX * numHexesWide * terrainLayer.hexHorizOffset;
    regionStartYPixel = regionY * numHexesTall * terrainLayer.hexPixelHeight;

    // Now clamp the width/height to prevent list overruns.
    num upperLeftHexX = regionUpperLeftHexX();
    num upperRightHexX = upperLeftHexX + numHexesWide;
    if(upperRightHexX > mapContainer.hexesWide) {
      num diff = upperRightHexX - mapContainer.hexesWide;
      numHexesWide -= diff;
    }

    num upperLeftHexY = regionUpperLeftHexY();
    num lowerRightHexY = upperLeftHexY + numHexesTall;
    if(lowerRightHexY > mapContainer.hexesTall) {
      num diff = lowerRightHexY - mapContainer.hexesTall;
      numHexesTall -= diff;
    }
  }

  int regionUpperLeftHexX() {
    return regionX * TerrainLayerSprite.regionSize;
  }

  int regionUpperLeftHexY() {
    return regionY * TerrainLayerSprite.regionSize;
  }

  double distanceToRegion(otherRegion) {
    int dX = otherRegion.regionX - regionX;
    int dY = otherRegion.regionY - regionY;
    return math.sqrt(math.pow(dX, 2) + math.pow(dY, 2));
  }

  void showIfNearRegion(TerrainLayerRegionSprite otherRegion) {
    double dist = distanceToRegion(otherRegion);
    if(dist < 3.0) {
      showRegion();
    } else {
      hideRegion();
    }
  }

  void showRegion() {
    if(isShown == true)
      return;
    terrainLayer.addChild(this);
    isShown = true;
  }

  void hideRegion() {
    if(isShown == false)
      return;
    terrainLayer.removeChild(this);
    isShown = false;
  }

  void drawAllHexes() {
    for(var y = 0; y < numHexesTall; y++) {
      for(var x = 0; x < numHexesWide; x++) {
        int hexX = regionUpperLeftHexX() + x;
        int hexY = regionUpperLeftHexY() + y;

        double hexUpperLeftXPixel = regionStartXPixel + (x * terrainLayer.hexHorizOffset);
        double hexUpperLeftYPixel = regionStartYPixel + (y * terrainLayer.hexPixelHeight);
        bool hexIsEven = (hexX & 1) == 0;
        if (hexIsEven) {
          hexUpperLeftYPixel += terrainLayer.hexPixelHeight / 2;
        }

        drawHex(hexUpperLeftXPixel, hexUpperLeftYPixel, hexX, hexY);
      }
    }
  }

  void drawHex(double hexUpperLeftXPixel, double hexUupperLeftYPixel,
               int hexX, int hexY) {
    String terrainChar = mapContainer.hexTerrain[hexY][hexX];
    String terrainName = mapContainer.terrainNames[terrainChar];
    String hexElev = mapContainer.hexElevations[hexY][hexX];
    String bitmapName = "${terrainName}${hexElev}";

    var bitmapData = terrainLayer._textureAtlas.getBitmapData(bitmapName);
    var bitmap = new Bitmap(bitmapData)
      ..x = hexUpperLeftXPixel
      ..y = hexUupperLeftYPixel
      // Adds the hex to the region Sprite.
      ..addTo(this);
  }

}