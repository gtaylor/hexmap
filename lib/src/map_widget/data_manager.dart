part of map_widget;

/**
 * This class handles the piping of data to/from the server. It loads
 * everything we need to render the map and keeps it up to date as
 * we proceed.
 */
class HexMapDataManager {
  static const String MAP_JSON_URL = 'data/currmap.json';

  MapContainer mapContainer;

  HexMapDataManager(MapContainer this.mapContainer) {
  }

  async.Future retrieveAndLoadAllMapData() {
    var completer = new async.Completer();
    html.HttpRequest.getString(MAP_JSON_URL)
      .then((responseText) {
        Map mapData = convert.JSON.decode(responseText);
        setMapData(mapData);
        completer.complete();
      });
    return completer.future;
  }

  void setMapData(Map mapData) {
    mapContainer.hexesWide = mapData['map_width'];
    mapContainer.hexesTall = mapData['map_height'];
    mapContainer.hexElevations = mapData['elevation'];
    mapContainer.hexTerrain = mapData['terrain'];
    mapContainer.terrainNames = mapData['terrain_names'];

    mapContainer.terrainLayer.calcAndSetPixelDimensions();
  }

}