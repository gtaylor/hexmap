part of map_widget;

/**
 * This is the top-level interactable map object. It uses various sub-objects
 * to manage map data, hex drawing, and input. All of the important data remains
 * on this class.
 */
class MapContainer extends DisplayObjectContainer {

  int hexesWide;
  int hexesTall;

  List<List> hexElevations;
  List<List> hexTerrain;
  Map terrainNames;

  ResourceManager resourceManager;
  Stage stage;
  UnitStore unitStore;
  EventBus eventBus;

  HexMapKeyboardInputHandler keyboardInputHandler;
  HexMapMouseInputHandler mouseInputHandler;
  TerrainLayerSprite terrainLayer;
  UnitLayerSprite unitLayer;
  HexMapDataManager dataManager;

  MapContainer(Stage this.stage, EventBus this.eventBus, UnitStore this.unitStore) {
    // Retrieves and sets the hexElevations/hexTerrain and other instance
    // attribs that rely on remote data.
    dataManager = new HexMapDataManager(this);
    // Renders and manages the terrain layer.
    terrainLayer = new TerrainLayerSprite(this);
  }

  /**
   * We can't do all of the setup work in the constructor because we have
   * to pull external assets and data.
   */
  async.Future setup() {
    var completer = new async.Completer();

    _loadResourceManager()
      .then((_) => dataManager.retrieveAndLoadAllMapData())
      // Uses the details in hexElevations and hexTerrain to draw hexes on-demand.
      .then((_) => terrainLayer.createRegions())
      .then((_) {
        x = pivotX = stage.contentRectangle.width / 2.0;
        y = pivotY = stage.contentRectangle.height / 2.0;
        keyboardInputHandler = new HexMapKeyboardInputHandler(this);
        mouseInputHandler = new HexMapMouseInputHandler(this);
        // Renders and manages units on the map.
        unitLayer = new UnitLayerSprite(this, unitStore);
        unitLayer.addAllFromStore();
        completer.complete();
      });

    return completer.future;
  }

  /**
   * Loads the hex textures and any other external assets.
   */
  async.Future _loadResourceManager() {
    resourceManager = new ResourceManager()
      ..addTextureAtlas(
          'hexTextures', 'img/hex_textures.json', TextureAtlasFormat.JSONARRAY)
      ..addTextureAtlas(
          'unitTextures', 'img/unit_textures.json', TextureAtlasFormat.JSONARRAY);
    return resourceManager.load();
  }

  /**
   * We inject our input handlers in here, which need to happen every frame.
   * This lets us respond to multiple keys pressed at once.
   */
  void render(RenderState renderState) {
    bool movedThisFrame = keyboardInputHandler.frame() || mouseInputHandler.frame();
    if(movedThisFrame) {
      terrainLayer.toggleRegions();
    }
    super.render(renderState);
  }
}