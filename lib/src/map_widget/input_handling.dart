part of map_widget;

/**
 * A light wrapper around the JavelinKeyboard class, which can handle
 * multiple keypresses and chords.
 */
class HexMapKeyboardInputHandler {
  JavelinKeyboard jKeyboard;
  MapContainer mapContainer;

  HexMapKeyboardInputHandler(MapContainer this.mapContainer) {
    jKeyboard = new JavelinKeyboard();

    mapContainer.addEventListener(KeyboardEvent.KEY_DOWN, _onKeyDown);
    mapContainer.addEventListener(KeyboardEvent.KEY_UP, _onKeyUp);
  }

  void _onKeyDown(KeyboardEvent e) {
    e.stopPropagation();
   jKeyboard.keyboardEvent(e, true);
  }

  void _onKeyUp(KeyboardEvent e) {
    e.stopPropagation();
    jKeyboard.keyboardEvent(e, false);
  }

  /**
   * This is called on every single frame, so be careful what goes in here!
   */
  bool frame() {
    bool movedThisFrame = false;

    jKeyboard.frame();

    double moveIncrement = 5.0;
    if (jKeyboard.isHeld(JavelinKeyCodes.KeyShift)) {
      moveIncrement += 35.0;
    }

    if (jKeyboard.isHeld(JavelinKeyCodes.KeyRight)) {
      mapContainer.terrainLayer.panX(moveIncrement);
      movedThisFrame = true;
    }
    if (jKeyboard.isHeld(JavelinKeyCodes.KeyLeft)) {
      mapContainer.terrainLayer.panX(-moveIncrement);
      movedThisFrame = true;
    }
    if (jKeyboard.isHeld(JavelinKeyCodes.KeyDown)) {
      mapContainer.terrainLayer.panY(moveIncrement);
      movedThisFrame = true;
    }
    if (jKeyboard.isHeld(JavelinKeyCodes.KeyUp)) {
      mapContainer.terrainLayer.panY(-moveIncrement);
      movedThisFrame = true;
    }
    return movedThisFrame;
  }
}


class HexMapMouseInputHandler {
  JavelinMouse jMouse;
  MapContainer mapContainer;

  HexMapMouseInputHandler(MapContainer this.mapContainer) {
    jMouse = new JavelinMouse();

    mapContainer.addEventListener(MouseEvent.MOUSE_MOVE, _onMouseMove);
    mapContainer.addEventListener(MouseEvent.MOUSE_DOWN, _onMouseLeftMouseClick);
    mapContainer.addEventListener(MouseEvent.MOUSE_UP, _onMouseLeftMouseUp);
    mapContainer.addEventListener(MouseEvent.MOUSE_WHEEL, _onMouseScroll);
  }

  void _onMouseMove(MouseEvent e) {
   jMouse.mouseMoveEvent(e);
  }
  void _onMouseLeftMouseClick(MouseEvent e) {
    jMouse.mouseButtonEvent(JavelinMouseButtonCodes.MouseButtonLeft, true);
  }
  void _onMouseLeftMouseUp(MouseEvent e) {
    jMouse.mouseButtonEvent(JavelinMouseButtonCodes.MouseButtonLeft, false);
  }
  void _onMouseScroll(MouseEvent e) {
    e.stopPropagation();
    jMouse.mouseScrollEvent(e);
  }

  bool frame() {
    bool movedThisFrame = false;
    if(jMouse.accumulatedDX != 0.0) {
      mapContainer.terrainLayer.panX(jMouse.accumulatedDX);
      movedThisFrame = true;
    }
    if(jMouse.accumulatedDY != 0.0) {
      mapContainer.terrainLayer.panY(jMouse.accumulatedDY);
      movedThisFrame = true;
    }

    if(jMouse.scrollDY < 0.0) {
      Point mouse = new Point(jMouse.X, jMouse.Y);
      mapContainer.terrainLayer.modScale(mouse, 0.035);
      movedThisFrame = true;
    } else if(jMouse.scrollDY > 0.0) {
      Point mouse = new Point(jMouse.X, jMouse.Y);
      mapContainer.terrainLayer.modScale(mouse, -0.035);
      movedThisFrame = true;
    }

    jMouse.resetAccumulator();
    return movedThisFrame;
  }
}