library hexmap;

import 'dart:html' as html;
import 'dart:async' as async;
import 'package:event_bus/event_bus.dart';
import 'package:stagexl/stagexl.dart';
import "package:hexmap/src/unit_store/unit_store.dart";
import "package:hexmap/src/map_widget/map_widget.dart";

html.Element stageElement = html.querySelector('#stage');
Stage stage = new Stage(stageElement, webGL: true);
ResourceManager resourceManager = new ResourceManager();
EventBus eventBus = new EventBus();

void main() {
  RenderLoop renderLoop = new RenderLoop()
    ..addStage(stage);

  UnitStore unitStore = new UnitStore(eventBus);
  var socket = new html.WebSocket('ws://127.0.0.1:9000', ["wamp"]);
  var client = new BattleSnakeClient(socket, unitStore);

  MapContainer mapContainer = new MapContainer(stage, eventBus, unitStore);
  mapContainer.setup()
    .then((resourceManager) {
      stage.addChild(mapContainer);
      stage.focus = mapContainer;
      stageElement.focus();
    });

}