import simplejson
from btmux_maplib.constants import TERRAIN_NAMES
from btmux_maplib.map_parsers.fileobj import MapFileObjParser

fobj = open('../web/data/currmap.map')

parser = MapFileObjParser(fobj)
mapobj = parser.get_muxmap()

mapdat = {
    'terrain': mapobj.terrain_list,
    'elevation': mapobj.elevation_list,
    'terrain_names': TERRAIN_NAMES,
    'map_width': mapobj.get_map_width(),
    'map_height': mapobj.get_map_height(),
}

out_fobj = open('../web/data/currmap.json', 'w')
out_fobj.write(simplejson.dumps(mapdat))
