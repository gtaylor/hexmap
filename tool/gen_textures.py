import os
from subprocess import call

from btmux_maplib.constants import TERRAIN_NAMES, ELEVATIONS

from texture_gen.hex_drawer import draw_hex


HEX_SIZE = 30.0
OUTPUT_PATH = os.path.join('texture_gen', 'textures')

for elevation in ELEVATIONS:
    for terrain_char, terrain_name in TERRAIN_NAMES.items():
        img = draw_hex(elevation, terrain_char, terrain_name, HEX_SIZE)
        filename = "%s%d.png" % (terrain_name, elevation)
        filepath = os.path.join(OUTPUT_PATH, filename)
        img.save(filepath, "PNG")

call([
    "TexturePacker",
    "texture_gen/textures",
    "--format", "json-array",
    "--sheet", "../web/img/hex_textures{v}.png",
    "--data", "../web/img/hex_textures.json",
    "--texture-format", "png",
    "--trim-sprite-names",
])
