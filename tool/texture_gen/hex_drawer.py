import math
from PIL import Image, ImageDraw, ImageFont
from btmux_maplib.img_generator.rgb_vals import cmap


def draw_hex(elevation, terrain_char, terrain_name, hex_size):
    hex_width = math.ceil(hex_size * 2.0)
    hex_height = math.ceil(math.sqrt(3) / 2 * hex_width)
    start_x = hex_width / 2.0
    start_y = hex_height / 2.0

    print "%s%d" % (terrain_name, elevation)

    points = []
    for pt in range(7):
        angle = 2 * math.pi / 6 * pt
        x_i = round(start_x + hex_size * math.cos(angle), 0)
        y_i = round(start_y + hex_size * math.sin(angle), 0)
        points.append((x_i, y_i))
    #print points

    img = Image.new("RGBA", (int(hex_width) + 2, int(hex_height) + 2), None)
    draw = ImageDraw.Draw(img)
    rgb_val = cmap[terrain_char][elevation]
    draw.polygon(points, outline="black", fill=rgb_val)

    # Elevation text
    if terrain_char == '.':
        terrain_char = ' '
    font = ImageFont.truetype("Courier New.ttf", size=12, filename="texture_gen/Courier New.ttf")

    fourth_char = str(elevation) if elevation > 0 else terrain_char
    draw.text((hex_width * 0.63, hex_height * 0.62), fourth_char, fill="black", font=font)
    draw.text((hex_width * 0.30, hex_height * 0.15), terrain_char, fill="black", font=font)
    draw.text((hex_width * 0.30, hex_height * 0.62), terrain_char, fill="black", font=font)
    draw.text((hex_width * 0.63, hex_height * 0.15), terrain_char, fill="black", font=font)
    return img
