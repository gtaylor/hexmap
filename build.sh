#!/bin/bash
rm -rf build

mkdir -p build/js
mkdir build/img
cp -r web/img/*.json web/img/*.png build/img/
mkdir build/data
cp -r web/data/*.json build/data/

cp web/*.css build
cp web/index.html build
dart2js web/hexmap.dart -o build/js/hexmap.js

